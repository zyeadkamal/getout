//
//  SettingsViewController.swift
//  Get Me Out
//
//  Created by Salah  on 28/05/2020.
//  Copyright © 2020 Salah . All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController {

  
    var settings = [Settings]()
    
    
    
    
    override func viewDidLoad() {
          super.viewDidLoad()
            // Do any additional setup after loading the view.
        settings.append(Settings(image: UIImage(systemName: "info.circle")!, label: "عن التطبيق"))
        settings.append(Settings(image: UIImage(systemName: "doc.plaintext")!, label: "سياسة الاستخدام"))
        settings.append(Settings(image: UIImage(systemName: "star")!, label: "تقييم التطبيق"))
        settings.append(Settings(image: UIImage(systemName: "arrow.right.square")!, label: "تسجيل الخروج"))
        tableView.separatorStyle = .none
        //tableView.rowHeight = 65
      }
    
    
    
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settings.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as! SettingsTableViewCell
        
        cell.settings = settings[indexPath.row]
        return cell
        
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 0:
            performSegue(withIdentifier: "", sender: self)
        case 1:
            performSegue(withIdentifier: "goToPolicy", sender: self)
        case 2:
            performSegue(withIdentifier: "", sender: self)
        case 3:
            performSegue(withIdentifier: "", sender: self)
        default:
            performSegue(withIdentifier: "", sender: self)
        }
    }
    
}


