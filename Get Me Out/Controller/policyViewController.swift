//
//  policyViewController.swift
//  Get Me Out
//
//  Created by zeyad on 6/13/20.
//  Copyright © 2020 Salah . All rights reserved.
//

import UIKit

class policyViewController: UIViewController {
    
    @IBOutlet weak var policyCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        policyCollectionView.delegate = self
        policyCollectionView.dataSource = self
        
    }
    
}

extension policyViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
  
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 1    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "policyCell", for: indexPath) as! policyCollectionViewCell
        
        cell.textLabel.text = "a7aaaaaaaaaaaaaasaksmxksxmskxmksxmkxmakxmaskxmsakxmaskxm"
        cell.layer.cornerRadius = 10
        cell.layer.borderWidth = 0.3
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowRadius = 50.0
        cell.layer.shadowOpacity = 0.3
        cell.layer.masksToBounds = false
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let bound = collectionView.bounds
        let height = self.view.frame.height
        let width = self.view.frame.width
        let cellSize = (height < width) ? bound.height : bound.width
        return CGSize(width: cellSize - 20  , height: (cellSize * 2) - 30  )

    }
    
    
}
