//
//  PolicyCollectionViewController.swift
//  Get Me Out
//
//  Created by zeyad on 6/13/20.
//  Copyright © 2020 Salah . All rights reserved.
//

import UIKit


class PolicyCollectionViewController: UICollectionViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

      
    }

   

    // MARK: UICollectionViewDataSource

  


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 14
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "policyCell", for: indexPath)
    
        // Configure the cell
    
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
              return UIEdgeInsets(top: 1, left: 5, bottom: 1, right: 5)
          }
          
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
              let bound = collectionView.bounds
              let height = self.view.frame.height
              let width = self.view.frame.width
              let cellSize = (height < width) ? bound.height/2 : bound.width/2
              return CGSize(width: cellSize - 5.5 , height: cellSize - 5.5)
              
          }
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
              return 0
          }
          
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
              return 1
          }



}
