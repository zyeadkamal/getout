//
//  CategoryHomeObject.swift
//  Get Me Out
//
//  Created by Salah  on 07/06/2020.
//  Copyright © 2020 Salah . All rights reserved.
//

import Foundation
struct CategoryHomeObject{
   var id: Int
   var seeMoreAPIURL: String
   var image: String
   var name: String
   var shortDesc: ShortDesc
   var imageurl: String
   var places: Places
}
